﻿# TDocker

## About
Monter un environnement “projet” avec des outils d’intégration continue et des machines “dockerisées”
Lien du sujet : https://docs.google.com/document/d/e/2PACX-1vQDgNrXwVKHtCovhYUqkzhgDmpFvdnrDpOtn0GSOI6axWtZbVEees1de7lkjJp4G4RL2Y4Yfz_U8A2F/pub

## Authors

Baptiste ROUSSEL G5
FRANÇOIS Valentin G5
GUILLAUME MAXIME G3
trochon LOÏC G5

### Présentation :

Notre projet consiste à la réalisation d'un jeu de rôle à choix sur le Web.
Nous utilisons pour ce faire du PHP et des templates HTML pour générer le contenu.

### Déroulement :

Au départ, l'utilisateur est amené à choisir un nom de personnage.
Ensuite le jeu débute, le joueur est amené à faire ses premiers choix.
Chaque choix dirige vers un certain scénario, il y a en fait plusieurs scénarii possibles.

Au fil du jeu, l'utilisateur aura des objets dans son inventaire. 
Ces objets auront un rôle déterminant pour la suite de l'histoire.