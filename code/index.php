<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Histoire interactive</title>
        <link type="text/css" rel="stylesheet" href="style/normalize.css">
        <link type="text/css" rel="stylesheet" href="style/style.css"> <?php //Ajout du css?>
    </head>
    <body>
        <header>
            <img id="pp" src="../images/gobilin.png">
            <h1 id="Nom">Jérome</h1>
            <ul id="info">
                <li><span class="bold" >Âge :</span> 30ans</li>
                <li><span class="bold" >Genre :</span> masculin</li>
                <li><span class="bold" >Métier :</span> Vendeur de Tapis</li>
            </ul>
        </header>
        <main>
            <section id="histoire">
                <?php 
                    $json = file_get_contents("data/histoire.json");    //On amène le chemin de la base de donnée en json
                    $mabaseJson = json_decode($json);                   //On décode la base de donnée
    
                    if(empty($_POST['Choix'])) //Get avant d'avoir fait la première requête //ce POST fixe
                    {
                        $start = $mabaseJson->{'tableau'}[0]->{'1'};    //On stock sur la variable start la premiere ligne du tableau
                        ?>
                            <h3><?php echo $start[0]->{'Text'};?></h3>
                            <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                                <button <?php /*choix 1*/?>
                                    <?php if($start[1]->{'Reponse'}[0]->{'1'}[1]->{'Liens'}==""){echo 'disabled="disabled"';} /*recuperation du data pour le premier tour*/ ?>     
                                    type="submit" name="Choix" value="<?php echo $start[1]->{'Reponse'}[0]->{'1'}[1]->{'Liens'}; /*Recuperation du lien dans value*/ ?>">     
                                        <?php echo $start[1]->{'Reponse'}[0]->{'1'}[0]->{'Text'}; /*Libéler du message dans le bouton*/ ?>     
                                </button>
                                <button <?php /*choix 2*/?>
                                    <?php if($start[1]->{'Reponse'}[1]->{'2'}[1]->{'Liens'}==""){echo 'disabled="disabled"';} ?>     
                                    type="submit" name="Choix" value="<?php echo $start[1]->{'Reponse'}[1]->{'1'}[1]->{'Liens'};?>">     
                                        <?php echo $start[1]->{'Reponse'}[1]->{'2'}[0]->{'Text'};?>     
                                </button>
                                <button <?php /*choix 3*/?>
                                    <?php if($start[1]->{'Reponse'}[2]->{'3'}[1]->{'Liens'}==""){echo 'disabled="disabled"';} ?>     
                                    type="submit" name="Choix" value="<?php echo $start[1]->{'Reponse'}[2]->{'1'}[1]->{'Liens'};?>">     
                                        <?php echo $start[1]->{'Reponse'}[2]->{'3'}[0]->{'Text'};?>     
                                </button>
                            </form>
                        <?php
                    }
                    if(isset($_POST['Choix'])) //Recupere le GET, et fait des POST avec les liens du json //Les posts sont variables
                    {
                        $data = $mabaseJson->{'tableau'}[ ((int)$_POST['Choix'])-1 ]->{$_POST['Choix']}; //On utilise lien du précédant POST pour récuperer d'autre donnée
                        ?>
                            <h3><?php echo $data[0]->{'Text'};?></h3>
                            <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                                <button <?php /*choix 1*/?>
                                    <?php if($data[1]->{'Reponse'}[0]->{'1'}[1]->{'Liens'}==""){echo 'disabled="disabled"';} /*recuperation de data pour les données pour le suivant*/ ?>     
                                    type="submit" name="Choix" value="<?php echo $data[1]->{'Reponse'}[0]->{'1'}[1]->{'Liens'}; /*Recuperation du lien dans value*/ ?>">
                                        <?php echo $data[1]->{'Reponse'}[0]->{'1'}[0]->{'Text'}; /*Libéler du message dans le bouton*/ ?>     
                                </button>
                                <button <?php /*choix 2*/?>
                                    <?php if($data[1]->{'Reponse'}[1]->{'2'}[1]->{'Liens'}==""){echo 'disabled="disabled"';} ?>     
                                    type="submit" name="Choix" value="<?php echo $data[1]->{'Reponse'}[1]->{'2'}[1]->{'Liens'};?>">     
                                        <?php echo $data[1]->{'Reponse'}[1]->{'2'}[0]->{'Text'};?>     
                                </button>
                                <button <?php /*choix 3*/?>
                                    <?php if($data[1]->{'Reponse'}[2]->{'3'}[1]->{'Liens'}==""){echo 'disabled="disabled"';} ?>     
                                    type="submit" name="Choix" value="<?php echo $data[1]->{'Reponse'}[2]->{'3'}[1]->{'Liens'};?>">     
                                        <?php echo $data[1]->{'Reponse'}[2]->{'3'}[0]->{'Text'};?>
                                </button>
                            </form>
                        <?php
                    }
                ?>
            </section>
        </main>
    </body>
</html>